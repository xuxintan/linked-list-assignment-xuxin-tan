#################task 1
#########################create a linked list
class Node(object):
    def __init__(self,num):
        self.num=num
        self.next=None
        self.pre=None
           
    def insert_in_the_middle(self,num):
       a=Node(num)
       a.next=self.next
       self.next=a
       
       
############task 2
########################################test time complexity of the linked list in different sizes

import time
samples=[2000,4000,8000,16000,32000]
linked_lists_set=[]
time_complexity_linked_lists=[]
for i in samples:
      a=Node(0)
      for j in range(i):
            a.insert_in_the_middle(j)
      linked_lists_set.append(a) ###create different size of linked lists
for linked_list in linked_lists_set:
      start=time.time()
      for num in range(5000): ##########insert 5000 numbers
          linked_list.insert_in_the_middle(num)
      end=time.time()
      time_=(end-start)
      time_complexity_linked_lists.append(time_/5000) #calculate the average time of insertion
print("time_complexity_linked_lists:", time_complexity_linked_lists)
#####the time complexity of the linked list is constant


######compare linked list with array list
########################################test time complexity of the array list
samples=[2000,4000,8000,16000,32000]
time_complexity_array_lists=[]
for i in samples:
    array_list=list(range(i))
    start=time.time()
    for num in range(5000):  ###create different size of linked lists
        array_list.insert(1,num)
    end=time.time()
    time_=(end-start)
    time_complexity_array_lists.append(time_/5000) #calculate the average time of insertion

print("time_complexity_array_lists:", time_complexity_array_lists)

##############Show the grafics
import matplotlib.pyplot as plt
plt.plot(samples, time_complexity_linked_lists, label = "linked list")
plt.plot(samples, time_complexity_array_lists, label = "array list")
plt.legend()
plt.show()
